// useFetch.js
import { useState } from "react";

const useApi = () => {
  const [data, setData] = useState(null);
  const [error, setError] = useState("");

  const fetchData = async (url, method, body) => {
    try {
      const response = await fetch(url, {
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      setData(result);
      return result; // Optionally, return the result if needed in the component
    } catch (error) {
      setError(error);
      throw error; // Rethrow the error to handle it in the component if needed
    }
  };

  return { data, error, fetchData };
};

export default useApi;
