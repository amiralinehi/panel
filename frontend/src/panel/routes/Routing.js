import {Routes,Route} from 'react-router-dom'
// components and pages

// user routes
import User from '../components/user/User'
import EditProfile from '../components/user/EditProfile'
import ChangePass from '../components/user/ChangePass'
// blog and news routes
import Blog from '../components/blog/Blog'
import Content from '../components/blog/Content'


// ticket
import Ticket from '../components/ticket/Ticket'
import Issuedtickets from '../components/ticket/IssuedTickets'
import CancelCharterFlights from '../components/ticket/CancelCharterFlights'
import PendingRequest from '../components/ticket/PendingConfirmRequests'
import CanceledTickets from '../components/ticket/CanceledTickets'
import PhoneRecord from '../components/ticket/PhoneRecord'
import SuccessfulPayment from '../components/ticket/SuccessfulPayment'
import UnSuccessfulPayment from '../components/ticket/UnSuccessfulPayment'
import AwaitingPaymentrequests from '../components/ticket/AwaitingPaymentRequests'
import PendingDeletedtRequests from '../components/ticket/PendingDeletetdRequests'
// hotel routes
import Hotel from '../components/hotel/Hotel'
import HotelStatus from '../components/hotel/HotelStatus'
import ReservedHotelSearch from '../components/hotel/ReservedHotelSearch'
import RulesManagement from '../components/hotel/RulesManagement'
import ContractManagement from '../components/hotel/ContractManagement'
import BrandsManagement from '../components/hotel/BrandsManagement'
import Extradition from '../components/hotel/Extradition'
import BaseInfo from '../components/hotel/BaseInfo'
import WebservicesSetting from '../components/hotel/WebservicesSetting'
import WebServicesFilters from '../components/hotel/webservicesFilters'

// reports routes
import Report from '../components/reports/Report'
import FullReport from '../components/reports/FullReport'
import DiagramReport from '../components/reports/DiagramReport'
import MemberSPurchasing from '../components/reports/MemberSPurchasing'
import SaleReport from '../components/reports/SaleReport'
import AirlinesReport from '../components/reports/AirlineReport'
// credit routes
import Credit from '../components/credit/Credit'
import Transactions from '../components/credit/Transactions'
import Deposit from '../components/credit/Deposit'

// home route
import Home from '../components/home/Home'

const Routing = ()=>{




    return(
    <Routes>
    <Route path='/home' element={<Home></Home>}></Route>

    {/* user */}
    <Route path='/user' element={<User></User>}></Route>
    <Route path='/user/edit' element={<EditProfile></EditProfile>}></Route>
    <Route path='/user/change' element={<ChangePass></ChangePass>}></Route>

    {/* ticket */}
    <Route path='/ticket' element={<Ticket></Ticket>}></Route>
    <Route path='/ticket/issued' element={<Issuedtickets></Issuedtickets>}></Route>
    <Route path='/ticket/cancel_charter' element={<CancelCharterFlights></CancelCharterFlights>}></Route>
    <Route path='/ticket/pending_confirm' element={<PendingRequest></PendingRequest>}></Route>
    <Route path='/ticket/canceled_tickets' element={<CanceledTickets></CanceledTickets>}></Route>
    <Route path='/ticket/phone_records' element={<PhoneRecord></PhoneRecord>}></Route>
    <Route path='/ticket/successful_payments' element={<SuccessfulPayment></SuccessfulPayment>}></Route>
    <Route path='/ticket/unsuccessful_payments' element={<UnSuccessfulPayment></UnSuccessfulPayment>}></Route>
    <Route path='/ticket/awaiting_payment_requests' element={<AwaitingPaymentrequests></AwaitingPaymentrequests>}></Route>
    <Route path='/ticket/pending_deleted_requests' element={<PendingDeletedtRequests></PendingDeletedtRequests>}></Route>

    {/* hotel */}
    <Route path='/hotel' element={<Hotel></Hotel>}></Route>
    <Route path='/hotel/status' element={<HotelStatus></HotelStatus>}></Route>
    <Route path='/hotel/reserved_hotel_search' element={<ReservedHotelSearch></ReservedHotelSearch>}></Route>
    <Route path='/hotel/rules_management' element={<RulesManagement></RulesManagement>}></Route>
    <Route path='/hotel/contract_management' element={<ContractManagement></ContractManagement>}></Route>
    <Route path='/hotel/brands_management' element={<BrandsManagement></BrandsManagement>}></Route>
    <Route path='/hotel/extradition' element={<Extradition></Extradition>}></Route>
    <Route path='/hotel/base_info' element={<BaseInfo></BaseInfo>}></Route>
    <Route path='/hotel/webservices_setting' element={<WebservicesSetting></WebservicesSetting>}></Route>
    <Route path='/hotel/webservices_filters' element={<WebServicesFilters></WebServicesFilters>}></Route>


    
    {/* blog */}
    <Route path='/blog' element={<Blog></Blog>}></Route>
    <Route path='/blog/content' element={<Content></Content>}></Route>


    {/* reports */}
    <Route path='/reports' element={<Report></Report>}></Route>
    <Route path='/reports/full_report' element={<FullReport></FullReport>}></Route>
    <Route path='/reports/diagram_report' element={<DiagramReport></DiagramReport>}></Route>
    <Route path='/reports/members_purchasing' element={<MemberSPurchasing></MemberSPurchasing>}></Route>
    <Route path='/reports/sale_report' element={<SaleReport></SaleReport>}></Route>
    <Route path='/reports/airline_reports' element={<AirlinesReport></AirlinesReport>}></Route>





    {/* credit */}
    <Route path='/credit' element={<Credit></Credit>}></Route>
    <Route path='/credit/transactions' element={<Transactions></Transactions>}></Route>
    <Route path='/credit/deposit' element={<Deposit></Deposit>}></Route>


    </Routes>
    )

}

export default Routing