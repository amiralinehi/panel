import BlogDate from "./BlogDate"
import BottomBar from "./bottomBar"
const BlogItem = ()=>{
    
    return(

        <div className="bg-white flex flex-col w-full h-fit gap-2 border-b-2 p-2 border-graay" dir="rtl">
        {/* date part */}
        <BlogDate></BlogDate>
        {/* title part */}
        <div className="bg-white text-text_moghiat w-fit font-semibold">کنسلی سیستمی</div>
        {/* larg text part */}
        <div className="text-sm text-text_moghiat bg-white" dir="rtl">
        طرح‌نما یا لورم ایپسوم به نوشتاری آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این نوشتار به‌عنوان عنصری از ترکیب‌بندی برای پُر کردن صفحه و ارائهٔ اولیهٔ شکل ظاهری و کلیِ طرح سفارش‌گرفته‌شده‌استفاده می‌کند، تا ازنظر گرافیکی نشانگر چگونگی نوع و اندازهٔ قلم و ظاهرِ متن باشد. ویکی‌پدیا
        </div>
        {/* bottom bar  */}
        <BottomBar></BottomBar>
        </div>
    )
}


export default BlogItem