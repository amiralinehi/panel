import { FaRegComments } from "react-icons/fa";
import { FaHeart } from "react-icons/fa";
import { MdOutlineRateReview } from "react-icons/md";



const BottomBar = ()=>{

    return(
        <div className="bg-blog_bottom_bar flex w-fit text-text_moghiat text-xs">
            <div className=" bg-blog_bottom_bar flex gap-4 pl-2">
                {/* comments */}
            <div className="flex gap-1 justify-center items-center" dir="rtl">
                    <FaRegComments></FaRegComments>
                    <div>{Math.round(Math.random()*100)}</div>
                    <div>نظر</div>
            </div>
            {/* likes */}
            <div className="flex gap-1 justify-center items-center" dir="rtl">
                    <FaHeart></FaHeart>
                    <div>{Math.round(Math.random()*100)}</div>
                    <div>likes</div>
            </div>
            {/*  */}
            <div className="flex gap-1 justify-center items-center" dir="rtl">
                    <MdOutlineRateReview></MdOutlineRateReview>
                    <div>{Math.round(Math.random()*100)}</div>
                    <div>بازدید</div>
            </div>


            </div>

            <div className="bg-blog_edame_matlab text-white p-1">ادامه مطلب</div>
        </div>
    )

}

export default BottomBar