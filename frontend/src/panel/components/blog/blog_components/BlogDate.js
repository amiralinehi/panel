const BlogDate = ()=>{

    return(
        <div className="flex flex-col w-16 justify-center items-center border-2 border-blog_item_date">
            <div className="bg-white text-blog_item_date w-full text-center">{Math.round(Math.random()*100)}</div>
            <div className="bg-blog_item_date text-white w-full text-center">month</div>
        </div>
    )

}

export default BlogDate