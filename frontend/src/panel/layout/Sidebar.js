import { useState } from "react";
import { FaCogs, FaCreditCard } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { FaTag } from "react-icons/fa6";
import { FaHouseChimney } from "react-icons/fa6";
import { BsPeopleFill } from "react-icons/bs";
import { MdOutlineViewList } from "react-icons/md";
import { MdAirplanemodeActive } from "react-icons/md";
import { IoBriefcaseOutline } from "react-icons/io5";
import { FaGlobeAmericas } from "react-icons/fa";

import SidebarOption from "./sidebar components/SidebarOption";
import SidebarSubOption from "./sidebar components/SidebarSubOption";



import {useNavigate} from 'react-router-dom'

// import { MdKeyboardArrowRight } from "react-icons/md";

const Sidebar = () => {
  const navigate = useNavigate()
  // const [sidebar, setSidebar] = useState(false);

  const [expandUserProfile, setExpandUserprofile] = useState(false);
  const [expandTicket, setExpandTicket] = useState(false);
  const [expandHotel, setExpandHotel] = useState(false);
  const [expandOptions, setExpandOptions] = useState(false);
  const [expandSetting, setExpandSetting] = useState(false);
  const [expandMemberManagement, setExpandMemberManagment] = useState(false);
  const [expandReports, setExpandReports] = useState(false);
  const [expandFlights, setExpandFlights] = useState(false);
  const [expandCredit, setExpandCredit] = useState(false);
  const [expandArchive, setExpandArchive] = useState(false);
  const [expandAgencyManagement, setExpandAgencyManagement] = useState(false);

  // const toggleSidebar = () => {
  //   setSidebar(!sidebar);
  // };

  // profile

  const toggleUserProfile = () => {
    setExpandUserprofile(!expandUserProfile);
    setExpandSetting(false);
    setExpandCredit(false)
    setExpandHotel(false)
    setExpandTicket(false)
    setExpandReports(false)
    navigate('/user')
  };
  // blog and news
  const toggleSetting = () => {
    setExpandSetting(!expandSetting);
    setExpandUserprofile(false);
    setExpandCredit(false)
    setExpandHotel(false)
    setExpandTicket(false)
    setExpandReports(false)
    navigate('/blog')
  };

  // hotel management
  const toggleHotel = () => {
    setExpandHotel(!expandHotel);
    setExpandSetting(false);
    setExpandUserprofile(false);
    setExpandCredit(false)
    setExpandTicket(false)
    setExpandReports(false)
    navigate('/hotel')
  };

  // ticket management
  const toggleTicket = () => {
    setExpandTicket(!expandTicket);
    setExpandHotel(false);
    setExpandSetting(false);
    setExpandUserprofile(false);
    setExpandCredit(false)
    setExpandReports(false)
    navigate('/ticket')
  };

  // options and settings
  const toggleOptions = () => {
    setExpandOptions(!expandOptions);
  };

  // manage members
  const toggleMemberManager = () => {
    setExpandMemberManagment(!expandMemberManagement);
  };

  // reports
  const toggleReports = () => {
    setExpandReports(!expandReports);
    setExpandHotel(false);
    setExpandSetting(false);
    setExpandUserprofile(false);
    setExpandCredit(false)
    setExpandTicket(false)
    navigate('/reports')
  };

  // system flights
  const toggleFlights = () => {
    setExpandFlights(!expandFlights);
  };

  // credit managment
  const toggleCredit = () => {
    setExpandCredit(!expandCredit);
    setExpandHotel(false);
    setExpandSetting(false);
    setExpandUserprofile(false);
    setExpandTicket(false)
    setExpandReports(false)
    navigate('/credit')
  };

  // archives
  const toggleArchives = () => {
    setExpandArchive(!expandArchive);
  };

  // agency
  const toggleAgencyManagement = () => {
    setExpandAgencyManagement(!expandAgencyManagement);
  };

  return (
    <>
      {/* <div className={`relative ${sidebar ? "w-72" : "w-7"}`}> */}

      <div className={`relative w-72`}>
        {/* parent of wholemenu items */}
        <div className="flex flex-col relative h-full justify-start bg-layout_background">
          {/* top gap */}
          {/* <div dir="rtl" className={`text-layout_background ${sidebar?'':'hidden'}`}> */}
          <div dir="rtl" className={`text-layout_background`}>
            <div>top-gap</div>
            <div>top-gap</div>
          </div>
          {/* menu item consist two parent section with one icon and name the other one is arrow icon */}

          {/* <div
            className={`flex justify-between items-center bg-sidebar_text_background p-0 mb-0 mt-1  ${
              sidebar ? "" : "hidden"
            }`}
            onClick={toggleUserProfile}
          > */}

          {/* first item */}
          <SidebarOption
            title={`پروفایل`}
            onClick={toggleUserProfile}
            expand={expandUserProfile}
            icon={FaUser}
          ></SidebarOption>

          {/* expanded user profile item 1*/}
          <SidebarSubOption
            title={`تغییر اطلاعات پروفایل`}
            expand={expandUserProfile}
            url={'/user/edit'}
          ></SidebarSubOption>

          {/* expanded user profile item 2*/}
          <SidebarSubOption
            title={`تغییر رمز ورود`}
            expand={expandUserProfile}
            url={'/user/change'}

          ></SidebarSubOption>

          {/* second item / ticket manager */}

          <SidebarOption
            title={`مدیریت بلیت`}
            onClick={toggleTicket}
            expand={expandTicket}
            icon={FaTag}
          ></SidebarOption>

          {/* ticket manger sub menu */}

          <SidebarSubOption
            title={`بلیت های صادر شده`}
            expand={expandTicket}
            url={'/ticket/issued'}
          ></SidebarSubOption>

          {/* expanded  ticket item 2*/}

          <SidebarSubOption
            title={`کنسل کردن بلیت های چارتری`}
            expand={expandTicket}
            url={'ticket/cancel_charter/'}
          ></SidebarSubOption>

          {/* expanded  ticket item 3*/}
          <SidebarSubOption
            title={`درخواست های منتظر تایید`}
            expand={expandTicket}
            url={'/ticket/pending_confirm'}
          ></SidebarSubOption>

          {/* expanded  ticket item 4*/}
          <SidebarSubOption
            title={`بلیت های کنسل شده`}
            expand={expandTicket}
            url={'/ticket/canceled_tickets'}
          ></SidebarSubOption>

          {/* expanded  ticket item 5*/}
          <SidebarSubOption
            title={`ثبت تلفنی`}
            expand={expandTicket}
            url={'/ticket/phone_records'}
          ></SidebarSubOption>

          {/* expanded  ticket item 6*/}
          <SidebarSubOption
            title={`پرداخت های موفق`}
            expand={expandTicket}
            url={'/ticket/successful_payments'}
          ></SidebarSubOption>

          {/* expanded  ticket item  7*/}
          <SidebarSubOption
            title={`پرداخت های ناموفق`}
            expand={expandTicket}
            url={'/ticket/unsuccessful_payments'}

          ></SidebarSubOption>

          {/* expanded  ticket item 8*/}
          <SidebarSubOption
            title={`درخواست های منتظر پرداخت`}
            expand={expandTicket}
            url={'/ticket/awaiting_payment_requests'}
          ></SidebarSubOption>

          {/* expanded  ticket item 9*/}

          <SidebarSubOption
            title={`درخواست های منتظر تایید حذف شده`}
            expand={expandTicket}
            url={'/ticket/pending_deleted_requests'}
          ></SidebarSubOption>

          {/* third item / hotel manager */}

          <SidebarOption
            title={`مدیریت هتل`}
            onClick={toggleHotel}
            expand={expandHotel}
            icon={FaHouseChimney}
          ></SidebarOption>


          <SidebarSubOption 
          title={`وضعیت هتل `}
          expand={expandHotel}
          url={'/hotel/status'}>
          </SidebarSubOption>


          <SidebarSubOption 
          title={`جستجو رزرو هتل`}
          expand={expandHotel}
          url={'/hotel/reserved_hotel_search'}>
          </SidebarSubOption>



          <SidebarSubOption 
          title={`مدیریت قوانین`}
          expand={expandHotel}
          url={'/hotel/rules_management'}>
          </SidebarSubOption>




          <SidebarSubOption 
          title={`مدیریت قرارداد`}
          expand={expandHotel}
          url={'/hotel/contract_management'}>
          </SidebarSubOption>




          <SidebarSubOption 
          title={`مدیریت نمادها`}
          expand={expandHotel}
          url={'/hotel/brands_management'}>
          </SidebarSubOption>


          <SidebarSubOption 
          title={`قوانین استرداد`}
          expand={expandHotel}
          url={'/hotel/extradition'}>
          </SidebarSubOption>



          <SidebarSubOption 
          title={`اطلاعات پایه`}
          expand={expandHotel}
          url={'/hotel/base_info'}>
          </SidebarSubOption>



          <SidebarSubOption 
          title={`تنظیمات وب سرویس`}
          expand={expandHotel}
          url={'/hotel/webservices_setting'}>
          </SidebarSubOption>




          <SidebarSubOption 
          title={`فیلتر هتل وب سرویس`}
          expand={expandHotel}
          url={'/hotel/webservices_filters'}>
          </SidebarSubOption>




          {/* forth item /site options */}

          <SidebarOption
            title={`تنظیمات سایت`}
            onClick={toggleOptions}
            expand={expandOptions}
            icon={FaCogs}
          ></SidebarOption>

          {/* fifth /blog and new */}

          <SidebarOption
            title={`مدیریت اخبار و بلاگ`}
            onClick={toggleSetting}
            expand={expandSetting}
            icon={FaCogs}
          ></SidebarOption>

          <SidebarSubOption
            title={`دسته بندی`}
            expand={expandSetting}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`محتوا`}
            expand={expandSetting}
            url={`/blog/content`}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`نظرات`}
            expand={expandSetting}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`آپلود عکس`}
            expand={expandSetting}
          ></SidebarSubOption>

          {/* sixth item / member managment */}

          <SidebarOption
            title={`مدیریت اعضا`}
            onClick={toggleMemberManager}
            expand={expandMemberManagement}
            icon={BsPeopleFill}
          ></SidebarOption>

          {/* seventh item /reports */}

          <SidebarOption
            title={`گزارشات`}
            onClick={toggleReports}
            expand={expandReports}
            icon={MdOutlineViewList}
          ></SidebarOption>

          <SidebarSubOption
            title={`گزارش جامع`}
            expand={expandReports}
            url={'reports/full_report'}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`گزارش نموداری`}
            expand={expandReports}
            url={'reports/diagram_report'}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`خرید اعضا`}
            expand={expandReports}
            url={'/reports/members_purchasing'}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`گزارش فروش`}
            expand={expandReports}
            url={'reports/sale_report'}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`گزارش ایرلاینی`}
            expand={expandReports}
            url={'/reports/airline_reports'}
          ></SidebarSubOption>

          {/* eights items */}

          <SidebarOption
            title={`پروازهای سیستمی`}
            onClick={toggleFlights}
            expand={expandFlights}
            icon={MdAirplanemodeActive}
          ></SidebarOption>

          {/* nineth item */}

          <SidebarOption
            title={`مدیریت مالی`}
            onClick={toggleCredit}
            expand={expandCredit}
            icon={FaCreditCard}
          ></SidebarOption>

          <SidebarSubOption
            title={`ریز تراکنش`}
            expand={expandCredit}
            url={'/credit/transactions'}
          ></SidebarSubOption>

          <SidebarSubOption
            title={`افزایش موجودی`}
            expand={expandCredit}
            url={'/credit/deposit'}
          ></SidebarSubOption>

          {/* tenth item */}

          <SidebarOption
            title={`آرشیو (بایگانی)`}
            onClick={toggleArchives}
            expand={expandArchive}
            icon={IoBriefcaseOutline}
          ></SidebarOption>

          {/* last item / agency management */}
          <SidebarOption
            title={`مدیریت نمایندگی`}
            onClick={toggleAgencyManagement}
            expand={expandAgencyManagement}
            icon={FaGlobeAmericas}
          ></SidebarOption>

          {/* button for support  */}
          <div className="w-full flex justify-center mt-2">
            <button
              className={`text-md font-semibold py-2 px-4 w-fit bg-gradient-to-r from-support_button_left_background to-support_button_background shadow-md rounded-2xl  text-white mt-2`}
            >
              پشتیبانی / Support
            </button>
          </div>
        </div>

        {/* <div className="absolute top-1" onClick={toggleSidebar}>
          <div className={`bg-white opacity-80 ${sidebar?'rounded-r-full':'rounded-full'}`}>
            <MdKeyboardArrowRight
              className={`text-title_background  ${
                sidebar ? "" : "rotate-180 transition-all delay-50"
              }`}

              size={21}
            ></MdKeyboardArrowRight>
          </div>
        </div> */}
      </div>
    </>
  );
};

export default Sidebar;
