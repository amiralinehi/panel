import NavItems from "./navigation components/Navitems"


const Navigation = () => {
    return (
      <div className="bg-nav_background">
        <NavItems></NavItems>
      </div>
    )
  }

export default Navigation
