import Navigation from "./Navigation";
import Sidebar from "./Sidebar";
import { FaHouseChimney } from "react-icons/fa6";
import { FaReadme } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { MdKeyboardArrowDown } from "react-icons/md";



// links
import { useLocation } from "react-router-dom";
import {Link} from 'react-router-dom'


const Wrapper = (props) => {

  let slug = useLocation()

  slug = slug.pathname.replace('/','')






  return (
    // entire wrapper background background color
    <div className="flex h-screen bg-layout_background">
      <div className="flex flex-col flex-grow relative">
        <Navigation></Navigation>
        <div className="flex-grow flex gap-0">
          {/* container for blog header before content */}
          <div className="flex flex-col justify-start items-end text-black bg-blog_container_bg w-full rounded-sm shadow-md px-4 mt-1">


            <div className=" bg-blog_container_bg w-full flex gap-2 p-2" dir="rtl">

              {/* home button  */}
                <Link to={`/home`} className=" text-nav_background bg-moghiat_button_bg rounded-md h-fit px-6 py-1 flex justify-center items-end gap-1">
                <FaHouseChimney></FaHouseChimney>
                <span>|</span>
                <div>خانه</div>
                </Link>

              {/* main page */}
                <Link to={`/user`}  className="text-nav_background bg-moghiat_button_bg rounded-md  h-fit px-6 py-1 flex justify-center items-end gap-1">
                <FaUser></FaUser>
                <span>|</span>
                  <div >کاربر</div>
                </Link>

                {/* announcements */}
                <Link to={`/blog`} className="text-nav_background bg-moghiat_button_bg rounded-md  h-fit px-6 py-1 flex justify-center items-end gap-1">
                <FaReadme></FaReadme>
                <span>|</span>
                  <div>وبلاگ</div>
                </Link>
            </div>

            {/* header of content */}
            <div className="flex justify-between items-center w-full bg-gradient-to-b from-anouncements_header to-header_bottom_color">
              {/* arrow left */}
              <div className="bg-header_bottom_color border-r border-moghiat_button_bg h-full px-2 py-1 flex items-center">
            <MdKeyboardArrowDown className="text-darkGray"></MdKeyboardArrowDown>
              </div>
              {/* text */}
            <div className=" shadow-lg w-full h-auto" dir="rtl"> موقعیت فعلی : {slug}</div>
            </div>
          {/* main content goes here depends on what route you take */}
            <div className="bg-white w-full flex-grow flex flex-col-reverse justify-end">
              {props.children}
              </div>
          </div>
          <Sidebar></Sidebar>
        </div>
        <footer className="bg-yellow-500 absolute bottom-0 w-full hidden">
          Footer
        </footer>
      </div>
    </div>
  );
};

export default Wrapper;
