// SidebarOption.js

import React from "react";
import { IoMdArrowDropleft } from "react-icons/io";

const SidebarOption = ({ title, onClick, expand, icon: IconComponent }) => {
  return (
    <div
      className={`flex justify-between items-center bg-sidebar_text_background p-0 mb-0 mt-1 cursor-pointer`}
      onClick={onClick}
    >
      {/* arrow */}
      <div className="flex justify-center items-center">
        <IoMdArrowDropleft
          className={`text-white ${
            expand
              ? " -rotate-90 transition-all delay-50"
              : "rotate-270 transition-all delay-50"
          }`}
        />
      </div>
      {/* user icon and text */}
      <div className="flex justify-center items-center gap-2 text-white bg-sidebar_text_background h-12">
        <div>{title}</div>
        <div className="flex bg-sidebar_icon_background items-center h-12 px-4">
          {IconComponent && <IconComponent size={21} />}
          {/* <FaCreditCard size={21} /> */}
        </div>
      </div>
    </div>
  );
};

export default SidebarOption;
