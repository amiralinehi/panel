// SidebarSubOption.js

import React from "react";
import { useNavigate} from 'react-router-dom'
const SidebarSubOption = ({ title, expand,url }) => {

  const navigate = useNavigate()

  return (
    <div
    onClick={()=>navigate(url)}
      className={`flex justify-end items-center bg-sidebar_text_background p-3 mb-0 border-b border-sidebar_icon_background hover:bg-sidebar_icon_background ${
        expand ? "" : "hidden"
      }`}
    >
      {/*text */}
      <div
        dir="rtl"
        className={`flex justify-center items-center gap-3 text-white`}
      >
        <div>{title}</div>
      </div>
    </div>
  );
};

export default SidebarSubOption;
