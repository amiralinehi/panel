//  icons for navigation items
import { FaBell } from "react-icons/fa";
import { FaEnvelope } from "react-icons/fa";
import { ImTrophy } from "react-icons/im";
import { FaUser } from "react-icons/fa";
import { IoMdArrowDropdown } from "react-icons/io";
import { FaMoneyBillAlt } from "react-icons/fa";
import { CiClock2 } from "react-icons/ci";
import { useState } from "react";
const NavItems = () => {
  const [showDropdown,setShowDropdown] =useState(false)
  return (
    <>
      {/* nav items container */}
      <div className="flex justify-between items-center">
        {/* left section */}
        <div className="flex gap-3 ml-4">
          {/* user */}
          <div onClick={()=>{setShowDropdown(!showDropdown)}} className="flex flex-col relative cursor-pointer">
            <div className="flex justify-center items-center gap-1">
            <IoMdArrowDropdown size={21} className={`text-nav_item_background rotate-180${showDropdown?'rotate-180 transition-all delay-50':''}`}></IoMdArrowDropdown>
            <div className="text-nav_item_background text-xs font-semibold">امیر محمد کارگر</div>
            <FaUser className="text-nav_item_background"></FaUser>
            </div>
            {/* drop down part */}
            <div className={`absolute bg-nav_background rounded-md py-1 px-2 top-10 left-1/6 w-full shadow-md ${showDropdown?'flex flex-col visible':'hidden'}`}>
            <div class="h-0 w-0 border-x-8 border-x-transparent border-b-[16px] border-b-nav_background -top-3 left-1 absolute"></div> 
              <ul className="flex justify-center items-center flex-col text-center">
                <li className="text-nav_item_background  w-full border-white hover:bg-nav_item_background hover:text-nav_background">test1</li>
                <li className="text-nav_item_background  w-full border-white hover:bg-nav_item_background hover:text-nav_background">test2</li>
                <li className="text-nav_item_background  w-full border-white hover:bg-nav_item_background hover:text-nav_background">test3</li>
              </ul>
            </div>
          </div>
          {/* envelope */}
          <div className="flex justify-center items-center">
            <FaMoneyBillAlt size={21} className="text-nav_item_background"></FaMoneyBillAlt>
          </div>

          {/* trophy */}
          <div className="flex justify-center items-center">
            <ImTrophy size={21} className="text-nav_item_background"></ImTrophy>
          </div>
        </div>

        {/* right section */}
        <div className="flex gap-3 ">
          {/* bell */}
          <div className="flex justify-center items-center text-graay">
            <FaBell size={21} className="text-nav_item_background"></FaBell>
          </div>

          {/* envelope */}
          <div className="flex justify-center items-center text-graay">
            <FaEnvelope size={21} className="text-nav_item_background"></FaEnvelope>
          </div>

          {/* clock */}
          <div className="flex justify-center items-center text-graay">
            <CiClock2 size={21} className="text-nav_item_background"></CiClock2>
          </div>

          {/* title part */}

          <div dir="rtl" className="bg-title_background flex p-3 w-64 text-nav_item_background font-semibold">
            چارتر ۱۱۸
          </div>
        </div>
      </div>
    </>
  );
};

export default NavItems;
