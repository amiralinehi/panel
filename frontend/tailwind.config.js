/* eslint-disable import/no-anonymous-default-export */
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        primary: "Vazir",
      },
      colors: {
        primary: "#a02777",
        darkPrimary: "#690e4a",
        darkestPrimary: "#5c083f",
        secondary: "#ffa903",
        light: "#f9fafb",
        graay: "#f2f2f2",
        darkGray: "#575757",
        skyDark: "#2aacd5",
        gg: "#818181",
        darkBlue: "#0f6698",
        pink: "#c873eb",
        darkestBlue: "#3782aa",
        // sidebar colors
        layout_background: "#32c2cd",
        sidebar_text_background:"#2eaeb9",
        sidebar_icon_background:"#229fa6",
        support_button_background:"#33bf3c",
        support_button_left_background:"#44db3f",
        title_background:"#3d3d3d",
        nav_item_background:"#9c9c9c",
        nav_background:"#4d4d4d",
        blog_edame_matlab:"#747474",
        button_blog:"#3bbccd",
        top_blog_header:"#f6f6f6",
        moghiat_button_bg:"#dbdbdb",
        text_moghiat:"#8a8a8a",
        anouncements_header:"#f7f7f7",
        blog_container_bg:"#f1f1f1",
        header_bottom_color:"#f3f3f3",
        blog_item_date:'#22c1c2',
        blog_bottom_bar:'#f2f2f2',

      },
    },
  },
  plugins: [],
};
